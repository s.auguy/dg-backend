package services

import data.BookRepositoryDatabase
import models.Book

import javax.inject.{Inject, Singleton}
import scala.concurrent.Future

@Singleton
class BookService @Inject() (bookRepository: BookRepositoryDatabase) {
  def listBooks(): Future[List[String]] = bookRepository.list()

  def getBook(isbn: String): Future[Option[Book]] = bookRepository.get(isbn)
}
