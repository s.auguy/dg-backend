package models

import play.api.libs.json.{Json, OWrites, Reads}

case class Book(isbn: String, title: String, appendixPage: String)

object Book {
  implicit val bookReads: Reads[Book] = Json.reads[Book]
  implicit val bookWrites: OWrites[Book] = Json.writes[Book]
}
