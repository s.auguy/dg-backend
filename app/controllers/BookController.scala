package controllers

import play.api.libs.json.Json
import play.api.mvc._
import services.BookService

import javax.inject._

@Singleton
class BookController @Inject() (
    implicit val controllerComponents: ControllerComponents,
    implicit val executionContext: scala.concurrent.ExecutionContext,
    val bookService: BookService
) extends BaseController {

  def index(): Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      bookService.listBooks().map(books => Ok(Json.toJson(books)))
  }

  def show(isbn: String): Action[AnyContent] = Action.async {
    implicit request: Request[AnyContent] =>
      bookService.getBook(isbn).map {
        case Some(book) => Ok(Json.toJson(book))
        case None       => NotFound
      }
  }
}
