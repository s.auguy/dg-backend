package data

import models.Book
import play.api.db.Database
import anorm._

import javax.inject.Inject
import scala.concurrent.Future

trait BookRepository {
  def list(): Future[List[String]]
  def get(isbn: String): Future[Option[Book]]
}

class BookRepositoryDatabase @Inject() (
    db: Database,
    implicit val databaseExecutionContext: DatabaseExecutionContext
) extends BookRepository {

  private val bookParser: RowParser[Book] = Macro.namedParser[Book]
  def list(): Future[List[String]] = {
    Future {
      db.withConnection { implicit conn =>
        SQL("SELECT isbn FROM book").as(SqlParser.str("isbn").*)
      }
    }
  }

  override def get(isbn: String): Future[Option[Book]] = {
    Future {
      db.withConnection { implicit conn =>
        val res = SQL("SELECT * FROM book WHERE isbn = {isbn}")
          .on("isbn" -> isbn)
          .as(bookParser.*)
        res.headOption
      }
    }
  }
}
