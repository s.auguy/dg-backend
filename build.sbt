name := """dg-backend"""
organization := "net.auguy"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.11"

libraryDependencies ++= Seq(
  guice,
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test,
  jdbc,
  "mysql" % "mysql-connector-java" % "8.0.33",
  "org.playframework.anorm" %% "anorm" % "2.7.0",
  evolutions,
  "com.h2database" % "h2" % "2.1.214" % Test,
  "org.mockito" %% "mockito-scala-scalatest" % "1.17.12" % Test
)
