-- Books schema

-- !Ups
CREATE TABLE IF NOT EXISTS book (
    isbn varchar(13) UNIQUE NOT NULL,
    title varchar(255) NOT NULL,
    appendixPage varchar(255),
    PRIMARY KEY (isbn)
);

-- !Downs

DROP TABLE book;