# DG-Backend

## Description
This is the backend application for the De Gruyter coding challenge.
This application is written in Scala and uses the Play framework to provide a REST API with 2 endpoints:
- `/books` - returns a list of book ISBNs
- `/books/:isbn` - returns a single book

## How to run
To run this application, you need Docker on your machine. Once it is installed, you can start the application and a MySQL database with

```
docker compose up
```

Once the backend is running (http://localhost:9000), you can seed the database with dummy data, running this command.

```
mysql -p"a strong password" -D playdb < conf/db/seed.sql
```

Note: if you don't have mysql installed on your machine, you can use the mysql docker container 
```
docker exec -i dg-backend-db-1 mysql -p"a strong password" -D playdb < conf/db/seed.sql
```

Then, you can clone the frontend from `git@gitlab.com:s.auguy/there-is-a-light-that-never-goes-out.git`
and run the application with 
```
npm install
npm run dev
```

## How to test
You can run the tests with 
```
sbt test
```

Note: if you don't have sbt installed, you can use the sbt docker container 
```
docker exec dg-backend-app-1 sbt test
```

## Implementation

This is a REST api with a 
- `BookController`: handles the API requests and responses
- `BookService`: handles the business logic. For now, it's not really useful as it only calls the `BookRepository` to get the data, but I prefer to have this separation of concerns here, in case we need business logic later on
- `BookRepository`: connects the application with the data layer
- `Book`: the model for a book which describes how to serialize/deserialize a `Book` to JSON

## Improvements

- There is no data validation on the database. We could implement some validation rules, especially on the `isbn` field (as it is done on the frontend)
- There is no pagination for the `/books` endpoint. In a real application, we would have to paginate the results to avoid returning too much data at once
- There is no separation between a development and production environment. In a real application, we would have a different configuration for each environment (e.g. different database credentials)
- There is no monitoring or error tracking. This would be necessary for a production environment