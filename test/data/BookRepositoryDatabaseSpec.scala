package data

import akka.actor.ActorSystem
import models.Book
import org.scalatestplus.play.PlaySpec
import play.api.db.Database
import play.api.test.Helpers.{await, defaultAwaitTimeout}

class BookRepositoryDatabaseSpec extends PlaySpec with DatabaseSpecHelper {

  def createRepo(database: Database) = new BookRepositoryDatabase(
    database,
    new DatabaseExecutionContext(ActorSystem())
  )
  def withBooksInDatabase(test: BookRepositoryDatabase => Any): Unit = {
    withInMemoryDatabase { database =>
      insertBooks(database)
      test(createRepo(database))
    }
  }
  "repository" when {
    "some books are present in the database" should {
      "list ISBNs" in {
        withBooksInDatabase { repository =>
          await(repository.list()) mustBe List(
            "9780300267662",
            "9780300268478",
            "9780520392304",
            "9780520392314",
            "9783110545982",
            "9783110914672",
            "9783110914675"
          )
        }
      }
      "retrieve a book" in {
        withBooksInDatabase { repository =>
          val isbn = "9780300267662"
          val title = "Why Architecture Matters"
          val appendixPage = "15"

          await(repository.get(isbn)) mustBe Some(
            Book(isbn, title, appendixPage)
          )
          await(repository.get("X")) mustBe None
        }
      }
    }

    "the database is empty" should {
      "return an empty list" in {
        withInMemoryDatabase { database =>
          await(createRepo(database).list()) mustBe List.empty
        }
      }
      "returns None when searching for a book" in {
        withInMemoryDatabase { database =>
          await(createRepo(database).get("9780300267662")) mustBe None
        }
      }
    }
  }
}
