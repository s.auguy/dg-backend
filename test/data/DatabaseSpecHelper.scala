package data

import play.api.db.{Database, Databases}
import play.api.db.evolutions.Evolutions

import scala.io.Source
import scala.util.Using

trait DatabaseSpecHelper {
  def withInMemoryDatabase[T](
                               block: Database => T
                             ): Unit = {
    Databases.withInMemory(
      urlOptions = Map(
        "MODE" -> "MYSQL"
      ),
      config = Map(
        "logStatements" -> true
      )
    ) { database =>
      Evolutions.withEvolutions(database) {
        block(database)
      }
    }
  }

  def insertBooks(database: Database): Int =
    database
      .getConnection()
      .prepareStatement(
        Using.resource(
          Source.fromFile(
            System.getProperty("user.dir") + "/conf/db/seed.sql"
          )
        )(_.mkString)
      )
      .executeUpdate()

}
