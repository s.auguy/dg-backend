import models.Book
import org.mockito.scalatest.MockitoSugar
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneServerPerSuite
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.test.Helpers._
import play.api.{Application, Environment, Mode}
import services.BookService

import scala.concurrent.Future

class FunctionalSpec
    extends PlaySpec
    with GuiceOneServerPerSuite
    with MockitoSugar {

  val mockBookService: BookService = mock[BookService]

  override lazy val app: Application = new GuiceApplicationBuilder()
    .in(Environment.simple(mode = Mode.Dev))
    .configure("play.evolutions.enabled" -> false)
    .overrides(bind[BookService].to(mockBookService))
    .build()

  val wsClient: WSClient = app.injector.instanceOf[WSClient]
  val myPublicAddress = s"localhost:$port"

  "BookController" should {
    "give a list of ISBN numbers on /books" in {
      when(mockBookService.listBooks()) thenReturn Future.successful {
        List(
          "9780300267662",
          "9783110914672",
          "9783110914675"
        )
      }

      val booksEndpoint = s"http://$myPublicAddress/books"

      val response = await(wsClient.url(booksEndpoint).get())

      response.status mustBe OK

      response.json mustBe Json.toJson(
        List(
          "9780300267662",
          "9783110914672",
          "9783110914675"
        )
      )
    }

    "give a book details on /books/9780300268478" in {
      when(mockBookService.getBook("9780300268478")) thenReturn Future
        .successful(
          Some(
            Book(
              "9780300268478",
              "The Great New York Fire of 1776",
              "64"
            )
          )
        )

      val booksEndpoint = s"http://$myPublicAddress/books/9780300268478"

      val response = await(wsClient.url(booksEndpoint).get())

      response.status mustBe OK

      response.json mustBe Json.parse(
        """{ "isbn":"9780300268478", "title":"The Great New York Fire of 1776", "appendixPage": "64" }"""
      )
    }

    "returns 404 when a isbn is not found" in {
      when(mockBookService.getBook("1234567890")) thenReturn Future.successful(
        None
      )
      val booksEndpoint = s"http://$myPublicAddress/books/1234567890"

      val response = await(wsClient.url(booksEndpoint).get())

      response.status mustBe NOT_FOUND
    }
  }
}
